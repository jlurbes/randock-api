<?php

namespace AppBundle\Controller;

//require "vendor/abraham/twitteroauth/autoload.php";

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Abraham\TwitterOAuth\TwitterOAuth;


class TwitterController extends FOSRestController
{
	/**
     * @Rest\Get("/getTweets/{user}/{n}")
     */
    public function getAction($user, $n)
    {
        // 10 minutes File Caché 
        if (file_exists("twitter_".$user."_".$n.".data")) {
            $data = unserialize(file_get_contents("twitter_".$user."_".$n.".data"));
            if ($data['timestamp'] > time() - 10 * 60) {
                $tweets = $data['twitter_result'];
            }
        }
        
        if (empty($tweets)) { // cache doesn't exist or is older than 10 mins            

            $consumer_key = "upFiW1r0kL62GiiWHNJK8zcbM";
            $consumer_secret = "B3mX7zFnUAEk7Qo2cPzej5XJXXEvhxihDXA0Kxgmikx7YIrvVL";
            $access_token = "163764394-BK3T3BQrBswTzobY51LpT8CRBremAn2nqEG6e2AP";
            $access_token_secret = "4E1ksIS05bbpW11xGn2eVwYUQyGbWAnDIuT5NUdxnenJd";
            $connection = new TwitterOAuth($consumer_key, $consumer_secret, $access_token, $access_token_secret);
            $api_tweets = $connection->get("statuses/user_timeline",["screen_name" => $user, "count" => $n]);
            $tweets = array();
            foreach($api_tweets as $api_tweet){
                $tweets[] = $api_tweet->text;
                //$tweets[] = strtoupper($api_tweet->text);
            }
        
            $data = array ('twitter_result' => $tweets, 'timestamp' => time());
            file_put_contents("twitter_".$user."_".$n.".data", serialize($data));
        }
        if (empty($tweets)) {
			return new View("The user does not exist", Response::HTTP_NOT_FOUND);
		}
        return $tweets;
    }
}
