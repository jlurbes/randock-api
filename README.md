api-randock
===========

Los pasos de instalación son los siguientes:

1. Descomprimir el proyecto que está en un archivo .zip o que se puede descargar con "git clone https://jlurbes@bitbucket.org/jlurbes/randock-api.git"
2. Acceder a la carpeta del proyecto y ejecutar el comando "composer install".
3. La url de acceso al servicio REST en el caso de que, por ejemplo, tengamos un entorno XAMPP sería "http://localhost/randock-api/web/app_dev.php/getTweets/{nombre}/{n}", siendo {nombre} el nombre del Twitter de la cuenta de la que nos interesa obtener los tweets y {n} el número de tweets a obtener. Un ejemplo de llamada sería "http://localhost/api-randock2/web/app_dev.php/getTweets/Javutty/5"

Puntos de Interés:

1. El desarrollo se ha realizado con Symfony3 y se ha realizado en unas 4 horas.
2. Para las interacciones con Twitter, se ha utilizado la librería "TwitterOAuth".
3. Para la interfaz REST se a utilizado "FOSRestBundle".
4. Se ha implementado un sistema de caché basado en ficheros en el que por cada llamada a la función getTweets se guarda un fichero del tipo "twitter_{nombre}_{n}.data", en el que se guarda un timestamp del momento en el que se ha ejecutado junto con los datos obtenidos. Si han pasado menos de 10 minutos y se vuelve a hacer la llamada se recupera la información del fichero. Si han pasado más de 10 minutos se vuelve a llamar a la función getTweets.

Problemas:

1. Se ha desactivado la conversión a mayúsculas de los tweets ya que hay ciertos carácteres que al convertir a mayúsculas y enviar por json producían un error que cortaba la ejecución.

